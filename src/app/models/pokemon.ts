export class Pokemon {
    name: string
    order: number = 0
    type: any[]
    fron_url: string
    back_url: string


    constructor() {

    }
    public fillPokemon(name: string, order: number, type: any[], fron_url: string, back_url: string) {
        this.name = name
        this.order = order
        this.type = type
        this.fron_url = fron_url
        this.back_url = back_url
    }


}
