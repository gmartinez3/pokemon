import { Component, HostListener, OnInit, ɵConsole } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon-service.service';
import { NgxSpinnerService } from "ngx-spinner";
import { Pokemon } from 'src/app/models/pokemon';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
declare var $:any


@Component({
    selector: 'app-pokemon-list',
    templateUrl: './pokemon-list.component.html',
    styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
    constructor(private _pokemonService: PokemonService, private _spinner: NgxSpinnerService, private toastr: ToastrService, private route: ActivatedRoute, private router: Router) { }
    pokemonList: Array<any> = new Array<any>()
    filtredPokemonList: Array<any> = new Array<any>()
    tab: number = 1
    filterPokemonText: string
    selectedPokemon: Pokemon = new Pokemon()
    ngOnInit(): void {
        this.fetchFirstGeneration()
        this.router.events.subscribe(value => {
            if (value instanceof NavigationEnd) {
                this.tab = 1
            }
        });

    }
    ngAfterViewInit(){
        this.initFocus()
    }



    detectParams() {
        this.route.params.subscribe(data => {
            if (data?.pokemon) {
                this.pokemonList = this.filtredPokemonList.filter(pokemon => pokemon.name.toLowerCase().includes(data?.pokemon?.toLowerCase()))
                this.filterPokemonText = data?.pokemon
            }

        })

    }
    fetchFirstGeneration() {
        this._spinner.show()
        this._pokemonService.fetchFirstGeneration()
            .then(data => {
                if (data != null && (data.pokemon_species?.length ?? 0) > 0) {
                    this.pokemonList = data?.pokemon_species
                    this.filtredPokemonList = data?.pokemon_species
                    this.detectParams()

                }
                this._spinner.hide()
            })
            .catch(data => this.toastr.error("Request has been failed"))
    }
    checkList() {
        return this.pokemonList?.length > 0;
    }
    openModalPokemon(pokemon: Pokemon) {
        this.selectedPokemon = pokemon
        $("#modalPokemon").modal("show")
    }
    resetValue() {
        document.getElementById(this.tab.toString()).focus()
    }

    @HostListener('document:keydown', ['$event']) onKeydownHandler(e: KeyboardEvent) {
        if (document.getElementById("modalPokemon")?.classList.contains("show")) return
        if ([38, 40].indexOf(e.keyCode) > -1) {
            e.preventDefault();

            let element = document.getElementById(this.tab.toString())
            element?.classList.remove("focused")
            if (e.keyCode == 38 && this.tab != 1) { this.tab-- }
            if (e.keyCode == 40 && this.tab != this.pokemonList?.length) { this.tab++ }
            element = document.getElementById(this.tab.toString())
            element?.classList.add("focused")
            element?.focus()

        }
    }


    initFocus() {
        setTimeout(() => {
            if (this.pokemonList.length > 0) {
                document.getElementById(this.tab.toString()).focus()
            }
        }, 200);
    }





}
