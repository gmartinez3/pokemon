import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    constructor(private router: Router) { }

    ngOnInit(): void {
    }
    filterPokemon(pokemonString: string) {
        document.getElementById("pokemonSearch").blur()
        if(pokemonString.length>0) {
            this.router.navigate(["/pokemon",pokemonString])

        }else{
            this.router.navigate([""])
        }
    }

}
