import { Component, EventEmitter, HostListener, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon';
declare var $: any

@Component({
    selector: 'app-pokemon-modal',
    templateUrl: './pokemon-modal.component.html',
    styleUrls: ['./pokemon-modal.component.scss']
})
export class PokemonModalComponent implements OnInit {
    @Input() pokemon: Pokemon = new Pokemon()
    @Output() modalClosed = new EventEmitter()
    constructor(private router: Router) { }

    ngOnInit(): void {

    }
    closeModal() {
        $('#modalPokemon').modal("hide")

        this.modalClosed.emit()

    }
    @HostListener('document:keyup.escape', ['$event']) onKeydownHandler(evt: KeyboardEvent) {
        this.closeModal()
    }




}
