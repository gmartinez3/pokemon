import { EventEmitter, HostListener, Output } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon';
import { PokemonService } from 'src/app/services/pokemon-service.service';
import { NgxSpinnerService } from 'ngx-spinner'
import { ToastrService } from 'ngx-toastr'



@Component({
    selector: 'app-pokemon-container',
    templateUrl: './pokemon-container.component.html',
    styleUrls: ['./pokemon-container.component.scss']
})


export class PokemonContainerComponent {

    @Input() pokemonName: string
    @Input() index: number
    @Output() openModalPokemon = new EventEmitter<any>();
    toLoad: boolean = false
    pokemon: Pokemon = new Pokemon()
    constructor(private _pokemonService: PokemonService, private spinner: NgxSpinnerService, private toastr: ToastrService) { }


    ngOnInit(): void {
        this.fetchPokemon()
    }



    fetchPokemon() {
        this.spinner.show()
        this._pokemonService.readInfoPokemon(this.pokemonName)
            .then(data => {
                this.pokemon.fillPokemon(data?.name, data?.id, data?.types, data?.sprites.front_default, data?.sprites.back_default)
                this.spinner.hide()

            })
            .catch(data => this.toastr.error("Request has been failed"))

    }


    chekEnter() {
        this.viewModal()
    }
    viewModal() {
        this.openModalPokemon.emit(this.pokemon)
    }



    /*
    This is my custom lazy load but im not proud about it
    customLazyLoad() {
        let hasBeenReaded = false
        window.addEventListener('scroll', (e) => {
            let pokemonDiv = document.getElementById(String(this.index));
            let coords = pokemonDiv.getBoundingClientRect()
            if (
                coords.top >= 0 &&
                coords.left >= 0 &&
                coords.right <= (window.innerWidth || document.documentElement.clientWidth) &&
                coords.bottom <= (window.innerHeight + 3000 || document.documentElement.clientHeight) && !hasBeenReaded
            ) {
                hasBeenReaded = true
                this.fetchPokemon()
            }
        });

    } */


}
