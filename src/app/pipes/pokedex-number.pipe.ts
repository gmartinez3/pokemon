import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'pokedexNumber'
})
export class PokedexNumberPipe implements PipeTransform {



    transform(value: any): any {
        return "#"+this.pad(value, 4, '0');
    }
    private pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }
}
