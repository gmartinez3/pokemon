import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { PokemonContainerComponent } from './components/pokemon-container/pokemon-container.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { PokedexNumberPipe } from './pipes/pokedex-number.pipe';
import { PokemonModalComponent } from './components/pokemon-modal/pokemon-modal.component';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        PokemonContainerComponent,
        PokemonListComponent,
        PokedexNumberPipe,
        PokemonModalComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        NgxSpinnerModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot()


    ],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent]
})
export class AppModule { }
