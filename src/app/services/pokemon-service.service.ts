import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';





@Injectable({
    providedIn: 'root'
})
export class PokemonService {
    url_request = "https://pokeapi.co/api/v2/"
    constructor(private _http: HttpClient) { }




    public fetchFirstGeneration(): Promise<any> {
        return this._http.get(`${this.url_request}generation/1`).toPromise()
    }

    public readInfoPokemon(name: string): Promise<any> {
        return this._http.get(`${this.url_request}pokemon/${name}`).toPromise()
    }

}


